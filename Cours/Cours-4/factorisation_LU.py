import numpy as np
import scipy.linalg as nla

# Prendre une matrice L avec des 1 sur la diagonale
L = np.array([[1,0,0,0],[2,1,0,0],[-3,5,1,0],[1,0,2,1]])
# Pour U prendre des éléments non nuls sur la diagonale
U = np.array([[2,0,3,1],[0,4,1,2],[0,0,-3,5],[0,0,0,7]])

A = np.dot (L, U)

# Partant de A, retrouver L et U !

P = np.array([
       [0., 1., 0., 0.],
       [1., 0., 0., 0.],
       [0., 0., 1., 0.],
       [0., 0., 0., 1.]])
np.dot (P, A) # permute les lignes 1 et 2 de A

A1 = A
a11 = 2
m21 = 4/a11
m31 = -6/a11
m41 = 2/a11

M1 = np.array ([
       [1., 0., 0., 0.],
       [-m21, 1., 0., 0.],
       [-m31, 0., 1., 0.],
       [-m41, 0., 0., 1.]])

A2 = np.dot(M1, A1)

a22 = 4
m32 = 20/a22
m42 = 0/a22

M2 = np.array([
       [1., 0., 0., 0.],
       [0., 1., 0., 0.],
       [0., -m32, 1., 0.],
       [0., -m42, 0., 1.]])

A3 = np.dot (M2, A2)

a33 = -3
m43 = -6/a33

M3 = np.array([
       [1., 0., 0., 0.],
       [0., 1., 0., 0.],
       [0., 0., 1., 0.],
       [0., 0., -m43, 1.]])

A4 = np.dot (M3, A3)

LL = np.array ([
    [1, 0, 0, 0],
    [m21, 1, 0, 0],
    [m31, m32, 1, 0],
    [m41, m42, m43, 1]])
# Optimisations

A = np.array([
       [ 2,  0,  3,  1],
       [ 4,  4,  7,  4],
       [-6, 20, -7, 12],
       [ 2,  0, -3, 18]], dtype=np.float64, order='F')
x = np.array([3, -6, 2, 20], dtype=np.float64)
b = np.dot (A, x)

A,piv = nla.lu_factor(A, overwrite_a=True)





