import numpy as np
import scipy.linalg as nla

# M = np.array([[1,-1,4],[1,4,-2],[1,4,2],[1,-1,0]])
# Q, R = nla.qr (M)
# Lambda = np.diag([-74,38,2,42])

# On triche : on construit une matrice dont on connaît les valeurs propres

Lambda = np.diag([-74,38,2,42])
X = np.array([[1,-1,4,2],[1,4,-2,1],[1,4,2,3],[1,-1,0,6]])
# A = X . Lambda . X**(-1) est une diagonalisation de A
A = np.dot (X, np.dot (Lambda, nla.inv (X)))

# Méthode de la puissance : connaissant A, trouver un vecteur propre
# associé à la valeur propre la plus grande en valeur absolue
v = np.array([.09876, 1.17, -0.34, .12121])
for k in range (0,10) :
    v = np.dot (A, v)
    v = (1 / nla.norm(v,2)) * v

# Quotient de Rayleigh : on retrouve une approximation de -74 = lambda_1
np.dot (v, np.dot(A,v)) / (np.dot(v,v))



# Méthode de la puissance inverse (version naïve) :
mu = 37.6 # une approximation de lambda = 38
v = np.array([.09876, 1.17, -0.34, .12121])
B = nla.inv (A - mu * np.eye(4))
for k in range (0,10) :
    v = np.dot (B, v)
    v = (1 / nla.norm(v,2)) * v

# On a trouvé une approximation d'un vecteur propre associé à 
#   la valeur propre 38 de A
# Application du quotient de Rayleigh sur A donne une amélioration de mu
np.dot (v, np.dot(A,v)) / (np.dot(v,v))


# Méthode de la puissance inverse (version classique) :
mu = 37.6 # une approximation de lambda = 38
v = np.array([.09876, 1.17, -0.34, .12121])
M = A - mu * np.eye(4)
P, L, U = nla.lu (M)
# La suite ne marche que si P = I
for k in range (0, 10) :
    v = nla.solve_triangular (L, v, lower=True)
    v = nla.solve_triangular (U, v, lower=False)
    v = (1 / nla.norm(v,2)) * v

np.dot (v, np.dot(A,v)) / (np.dot(v,v))

# L'algorithme QR sur un exemple

Lambda = np.diag([-74,38,2,42])
X = np.array([[1,-1,4,2],[1,4,-2,1],[1,4,2,3],[1,-1,0,6]])
# A = X . Lambda . X**(-1) est une diagonalisation de A
A = np.dot (X, np.dot (Lambda, nla.inv (X)))
for k in range (0,100) :
    Q, R = nla.qr (A)
    A = np.dot (R, Q)

# Sur la diagonale de A, on retrouve des approx. des valeurs propres

